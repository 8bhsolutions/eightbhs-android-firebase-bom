const PLUGIN_ID = "eightbhs-android-firebase-bom";
let DEBUG = false;

let q, fs, pluginDeferral;

function log(message) {
    console.log(PLUGIN_ID + ": " + message);
}

function debug(message) {
    if (DEBUG) console.log(`DEBUG: ${message}`);
}

function onError(error) {
    log("ERROR: " + error);
    pluginDeferral.resolve();
}

function run() {
    try {
        fs = require("fs");
    } catch (e) {
        throw "Failed to load dependencies. If using cordova@6 CLI, ensure this plugin is installed with the --fetch option: " + e.toString();
    }

    const args = getArgs();
    if (args["verbose"]) {
        DEBUG = true;
        log("Debug logging enabled");
    }

    processPackages().then(pluginDeferral.resolve);
}

function processPackages() {
    const deferred = q.defer();

    const projPropertiesPath = `./platforms/android/project.properties`;

    let packageDefs = fs.readFileSync(projPropertiesPath).toString();
    let hasBomPackage = false;

    const packageRegex = /^(\s*cordova\.system\.library\.\d+=.*)(:USE-BOM)(\s|$)/gim;

    const matches = packageDefs.match(packageRegex) || [];
    matches.forEach((m) => {
        log("Found [" + m + "]");
        hasBomPackage = true;
    });

    // overwrite project.properties for found packages
    if (hasBomPackage) {
        packageDefs = packageDefs.replace(packageRegex, "$1$3"); // Exclude capture group 2 - [$2 = :USE-BOM]
        fs.writeFileSync(projPropertiesPath, packageDefs, "utf8");
        debug("Update project.properties with changes");
    }

    deferred.resolve();
    return deferred.promise;
}

function attempt(fn) {
    return function () {
        try {
            fn.apply(this, arguments);
        } catch (e) {
            onError("EXCEPTION: " + e.toString());
            if (DEBUG) throw e;
        }
    };
}

function getArgs() {
    const args = {};
    process.argv.slice(2, process.argv.length).forEach((arg) => {
        // long arg
        if (arg.slice(0, 2) === "--") {
            const longArg = arg.split("=");
            const longArgFlag = longArg[0].slice(2, longArg[0].length);
            const longArgValue = longArg.length > 1 ? longArg[1] : true;
            args[longArgFlag] = longArgValue;
        }
        // flags
        else if (arg[0] === "-") {
            const flags = arg.slice(1, arg.length).split("");
            flags.forEach((flag) => {
                args[flag] = true;
            });
        }
    });
    return args;
}

module.exports = function (ctx) {
    try {
        q = require("q");
        pluginDeferral = q.defer();
    } catch (e) {
        e.message = "Unable to load node module dependency 'q': " + e.message;
        log(e.message);
        throw e;
    }
    attempt(run)();
    return pluginDeferral.promise;
};
